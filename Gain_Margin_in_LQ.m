A = [1 1;0 1];
B = [0;1];
E = [1;1];
C = [1 0];
n = 10;
% white noise in states
w = wgn(1,n,-2);
% white noise in output
v = wgn(1,n,0);
% intensity of w.n in states
sigma = var(w);


Q = q*[1;1]*[1 1];
R = 1;
f = 2+sqrt(4+q);
d = 2+sqrt(4+sigma);
g = f*[1;1];
k = d*[1;1];

% Set the range of d and f to be 1~100
% Check the variation of gain margin in the range

d = 2:100;
f = 2:100;
subplot(3,1,1);
surf(d,f,1+1./(d'*f));
xlabel('Value of d');
ylabel('Value of f');
title('Upper boundary of gain margin');

subplot(3,1,2);
surf(d,f,1-(d'*ones(1,99)+f'*ones(1,99)-4*ones(99))./2./(d'*f));
xlabel('Value of d');
ylabel('Value of f');
title('Lower boundary of gain margin');

subplot(3,1,3);
surf(d,f,1./(d'*f)+(d'*ones(1,99)+f'*ones(1,99)-4*ones(99))./2./(d'*f));
xlabel('Value of d');
ylabel('Value of f');
title('Range of gain margin');

