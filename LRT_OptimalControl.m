% Parameters of the system
A = [0 1;-3 -4];
B = [0;1];
C = [2 1];
R = 1;
F = [50 10];


%% Plot of transfer function according to the variation of q
q_set = [10, 100, 1000, 10000];
s = tf('s');

for i = 1:4
    q = q_set(i);
    Q = [35;-61]*[35 -61]+q*[0;1]*[0 1];
    P = Riccati(A,C'*inv(R)*C,Q);
    K = P*C'*inv(R);
    L = F*inv(s*eye(2)-A+B*F+K*C)*K*C*inv(s*eye(2)-A)*B;
    bode(L);
    hold on;
end




%% Plot of (A-KC) eigenvalues according to the variation of q

q_set = linspace(1,10^4,100);
eigenTV = zeros(2,30);

for i = 1:length(q_set)
    q = q_set(i);
    Q = [35;-61]*[35 -61]+q*[0;1]*[0 1];
    P = Riccati(A,C'*inv(R)*C,Q);
    K = P*C'*inv(R);
    eigenTV(:,i) = sort(eig(A-K*C));
end

plot(q_set,eigenTV);
grid on;
xlabel('Values of q');
ylabel('Eigenvalues');
legend('\lambda_{min}','\lambda_{max}');