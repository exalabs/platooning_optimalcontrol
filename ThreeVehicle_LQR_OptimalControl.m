%  Three vehicles using LQR method to do the optimal control


VehicleNum = 3;

% mass of vehicles
m1 = 1;
m2 = 1;
m3 = 1;

%drag coefficients 
alpha1 = 1;
alpha2 = 1;
alpha3 = 1;


% A matrix and B matrix 
A = zeros(2*VehicleNum-1);
A(1,1) = -alpha1/m1;
A(3,3) = -alpha2/m2;
A(5,5) = -alpha3/m3;
A(2,1:3) = [1 0 -1];
A(4,3:5) = [1 0 -1];
A(5,5) = -1;


B = zeros(2*VehicleNum-1, VehicleNum);
B(1,1) = 1;
B(3,2) = 1;
B(5,3) = 1;


% Components in the cost function

Q = zeros(5);
Q(2,2) = 10;
Q(4,4) = 10;
R = eye(3);

K_hat = [1.263 2.494 -0.819 0.668 -0.444;
        -0.819 -1.826 1.638 1.826 -0.819;
        -0.444 -0.668 -0.819 -2.494 1.263];
  
%%
% Under intial condition case 1
x0 = [0 -4.2 0 2.1 0]';
TimeInterval =0:0.05:20;
N = size(TimeInterval,2);


x = zeros(5,N); 
dt = 0.05;
x(:,1) = x0;
for i = 1:N-1
    x(:,i+1) = x(:,i) + dt*(A-B*K_hat)*x(:,i);    
end

subplot(3,1,1);
plot(TimeInterval,x(1,:));
xlabel('Time(s)');
ylabel('Position 1');
grid on;

hold on;


subplot(3,1,2);
plot(TimeInterval,x(3,:));
xlabel('Time(s)');
ylabel('Position 2');
grid on;

subplot(3,1,3);
plot(TimeInterval,x(5,:));
xlabel('Time(s)');
ylabel('Position 3');
grid on;









%% Change from open road to closed ring road


Q = zeros(6);
Q(2,2) = 10;
Q(4,4) = 10;
Q(6,6) = 10;

R = eye(3);
    


% A matrix and B matrix 
A = zeros(2*VehicleNum);
A(1,1) = -alpha1/m1;
A(3,3) = -alpha2/m2;
A(5,5) = -alpha3/m3;
A(2,1:3) = [1 0 -1];
A(4,3:5) = [1 0 -1];
A(5,5) = -1;
A(6,5) = 1;
A(6,1) = -1;


B = zeros(2*VehicleNum, VehicleNum);
B(1,1) = 1;
B(3,2) = 1;
B(5,3) = 1;


W_hat = zeros(3,6);
W_hat(:,1:5) = K_hat;

% Under intial condition case 1
x0 = [0 -4.2 0 2.1 0 4.3]';
TimeInterval =0:0.05:20;
N = size(TimeInterval,2);


x = zeros(6,N); 
dt = 0.05;
x(:,1) = x0;
for i = 1:N-1
    x(:,i+1) = x(:,i) + dt*(A-B*W_hat)*x(:,i);    
end

subplot(3,1,1);
plot(TimeInterval,x(2,:));
xlabel('Time(s)');
ylabel('Velocity 1');
grid on;

hold on;


subplot(3,1,2);
plot(TimeInterval,x(4,:));
xlabel('Time(s)');
ylabel('Velocity 2');
grid on;

subplot(3,1,3);
plot(TimeInterval,x(5,:));
xlabel('Time(s)');
ylabel('Position 3');
grid on;


    