% By using cheap control for a system, check the variation of eigenvalues




% Parameters of the system
A = [0 1;-3 -4];
B = [0;1];
C = [2 1];
F = [50 10];
R_set = 1:1:8;

%% Plot of transfer function according to the variation of q

eigenTV = zeros(2,8);

for i = 1:8


%% Plot of (A-KC) eigenvalues according to the variation of q
    Q = [35;-61]*[35 -61];
    R = R_set(i);
    P = Riccati(A,B*2^i*B',Q);
    K = P*C'*inv(R);
    eigenTV(:,i) = sort(eig(A-K*C));
end

plot(R_set,eigenTV);
grid on;
xlabel('Reduction rate of R','Fontsize',16);
ylabel('Eigenvalues','Fontsize',16);
legend({'\lambda_{min}','\lambda_{max}'},'Fontsize',16);