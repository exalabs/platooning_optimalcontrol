% Platoon control by adding mean velocity deviation to cost function, try
% to find the optimal cluster number in platoon dynamics


carNum = 4;
alpha = 1;  % alpha - steady stage drag force
mass = 1;



hd = 1 ;
Tau = 0.5;

%% Parameters

r = 6;
q1 = 2;
q2 = 5;
q3 = 3;




aMatrix = [0 1 0;0 0 1;0 0 -1/Tau];
bMatrix = [0;0;1/Tau];
cMatrix = [-1 -hd 0;0 -1 0;0 0 -1];


% neighborDistanceCost =  X'*DM_Term_Matrix*X;




%% Design of the cost function
% Case 1: similar to the paper, only deviation of positions and mean
% inputs are penalized


% ================================================================================


Q_Matrix = [q1 0 0; 0 q2*(inv(hd))^2 0;0 0 q3*Tau];
R_Matrix = [r];



%% Calculate the cost functions
% cost function J = 1/2 * (x'Qx + y'Ey + f'Rf);



K = Riccati([aMatrix],bMatrix*inv(R_Matrix)*bMatrix',Q_Matrix);

% The corresponding control would be equal to
Gain_Matrix = inv(R_Matrix)*BMatrix'*K;


%%

x0 = zeros(3,1);


x0(2) = 3;

TimeInterval =0:0.05:20;


Desire_S = sin(TimeInterval);
Desire_V = cos(TimeInterval);
Desire_A = -sin(TimeInterval);


N = size(TimeInterval,2);

x = zeros(3,N);
dt = 0.05;
x(:,1) = x0;
for i = 1:N-1
    dx_1 = Desire_S(i) - x(1,i);
    dx_2 = Desire_V(i) - x(2,i);
    dx_3 = Desire_A(i) - x(3,i);
    Tg = i*dt;
    Time_Trans = [Tg^3/6 Tg^4/24 Tg^5/120;-Tg^2/2 -Tg^3/6 -Tg^4/24;Tg Tg^2/2 Tg^3/6];
    HOMatrix= inv(Time_Trans)*[Desire_S(i) Desire_V(i) Desire_A(i)]';
    dx_4 = HOMatrix(1);
    dx_5 = HOMatrix(2);
    dx_6 = HOMatrix(3);
    Time_Global_TrsMatrix = [1 Tg Tg^2/2 Tg^3/6 Tg^4/24 Tg^5/120;
                            0 -1 -Tg -Tg^2/2 -Tg^3/6 -Tg^4/24;
                            0 0 1 Tg Tg^2/2 Tg^3/6];
    Desire_states = Time_Global_TrsMatrix * [dx_1 dx_2 dx_3 dx_4 dx_5 dx_6]';
    Real_states = [dx_1 dx_2 dx_3]';
  
    x(:,i+1) = x(:,i) + dt*AMatrix*x(:,i) +BMatrix*Gain_Matrix*(Real_states-Desire_states);
end

%
figure(1);


for i = 1:1
    
    plot(TimeInterval,x(i,:),TimeInterval,Desire_S);
    hold on;
end









































%% The state space model

AMatrix = zeros(3*carNum);
for i = 1:3:3*carNum-2
    if rem(i,2) == 1
        AMatrix(i,i) = velocityDeviation_Matrix((i+1)/2,(i+1)/2);
    else
        AMatrix(i,i-1) = distanceDeviation_Matrix(i/2,i/2);
        AMatrix(i,i+1) = distanceDeviation_Matrix(i/2,i/2+1);
    end
end


% neighborDistanceCost =  X'*DM_Term_Matrix*X;
Input_Matrix = zeros(carNum*2-1,carNum);
Force = 1/mass*eye(carNum);
for i = 1:carNum*2-1
    if rem(i,2)==1
        Input_Matrix(i,:) = Force((i+1)/2,:);
    else
        Input_Matrix(i,:) = zeros(1,carNum);
    end
end




positionDeviation_costMatrix = zeros(carNum*2-1);
for i = 1:carNum*2-1
    if rem(i,2)== 0
        positionDeviation_costMatrix(i,i) = 10;
    else
        positionDeviation_costMatrix(i,i) = 0;
    end
end



%% Calculate the cost functions
% cost function J = 1/2 * (x'Qx + y'Ey + f'Rf);


Gain_Matrix = zeros(4,7,401);
for j = 1:1
    
    Q_Matrix = positionDeviation_costMatrix;
    R_Matrix = eye(carNum);
    BMatrix = Input_Matrix;
    for m = 1:401
        BMatrix(1,1) = 10*exp(-0.02*m);
        K = Riccati(AMatrix,BMatrix*inv(R_Matrix)*BMatrix',Q_Matrix);
        
        % The corresponding control would be equal to
        Gain_Matrix(:,:,m) = inv(R_Matrix)*BMatrix'*K;
    end
    
    
    %%
    
    x0 = zeros(2*carNum-1,1);
    % Initial conditions
    for i = 1:2*carNum-1
        if rem(i,2) == 1
            x0(i) = 0;
        else
            x0(i) = 6;
        end
    end
    
    x0(2) = 3;
    x0(4) = 7;
    
    TimeInterval =0:0.05:20;
    N = size(TimeInterval,2);
    
    
    x = zeros(2*carNum-1,N);
    dt = 0.05;
    x(:,1) = x0;
    for i = 1:N-1
        x(:,i+1) = x(:,i) + dt*(AMatrix-BMatrix*Gain_Matrix(:,:,i))*x(:,i);
    end
    
    %
    figure(1);
    
    for i = 1:2:2*carNum-1
        
        plot(TimeInterval,x(i,:));
        hold on;
    end
    xlabel('Time(s)','FontSize', 16);
    ylabel('Velocity Deviation','FontSize', 16);
    legend('Velocity deviation #1','Velocity deviation #2', 'Velocity deviation #3', 'Velocity deviation #4');
    % ca = sprintf('Coefficent %d', Cft);
    % h = legend(ca);
    % set(h,'FontSize',12);
    grid on;
    
    
    
    %
    figure(2);
    
    for i = 1:2:2*carNum-3
        plot(TimeInterval,x(i+1,:));
        hold on;
    end
    xlabel('Time(s)','FontSize', 16);
    ylabel('Position Deviation','FontSize', 16);
    legend('Position deviation #1','Position deviation #2', 'Position deviation #3');
    % ca = sprintf('Coefficent %d', Cft);
    % h = legend(ca);
    % set(h,'FontSize',12);
    grid on;
    
    
end



