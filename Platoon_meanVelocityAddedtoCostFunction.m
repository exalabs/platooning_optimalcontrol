% Platoon control by adding mean velocity deviation to cost function, try
% to find the optimal cluster number in platoon dynamics


carNum = 4;
alpha = 1;  % alpha - steady stage drag force
mass = 1;





%% The state space model


% position deviation from the desired values
distanceDeviation_Matrix = eye(carNum)+diag(-ones(carNum-1,1),1);

% velocity deviation from the desired values
velocityDeviation_Matrix = -alpha/mass*eye(carNum);


AMatrix = zeros(2*carNum-1);
for i = 1:2*carNum-1
    if rem(i,2) == 1
        AMatrix(i,i) = velocityDeviation_Matrix((i+1)/2,(i+1)/2);
    else
        AMatrix(i,i-1) = distanceDeviation_Matrix(i/2,i/2);
        AMatrix(i,i+1) = distanceDeviation_Matrix(i/2,i/2+1);
    end
end


% neighborDistanceCost =  X'*DM_Term_Matrix*X;
Input_Matrix = zeros(carNum*2-1,carNum);
Force = 1/mass*eye(carNum);
for i = 1:carNum*2-1
    if rem(i,2)==1
        Input_Matrix(i,:) = Force((i+1)/2,:);
    else
        Input_Matrix(i,:) = zeros(1,carNum);
    end
end


%%  Terms for cost functions

% deviation fromt the mean velocity
% meanDeviation_costMatrix = zeros(2*carNum-1);
% DeviationfromMeanCost = X'*DM_Term_Matrix*X;
%
%
% transMatrix1 = [1 0]'*ones(1,carNum);
% transMatrix2 = 1/mass*reshape(transMatrix1,[1, 2*carNum]);
%
% for i = 1:carNum*2-1
%     if rem(i,2)==1
%        meanDeviation_costMatrix(i,:) = 1/carNum*transMatrix2(1:end-1);
%        meanDeviation_costMatrix(i,i) = meanDeviation_costMatrix(i,i) + 1;
%     else
%        meanDeviation_costMatrix(i,:) = zeros(1,carNum*2-1);
%     end
% end
%


%% Design of the cost function
% Case 1: similar to the paper, only deviation of positions and mean
% inputs are penalized


% ================================================================================


positionDeviation_costMatrix = zeros(carNum*2-1);
for i = 1:carNum*2-1
    if rem(i,2)== 0
        positionDeviation_costMatrix(i,i) = 10;
    else
        positionDeviation_costMatrix(i,i) = 0;
    end
end



%% Calculate the cost functions
% cost function J = 1/2 * (x'Qx + y'Ey + f'Rf);


Gain_Matrix = zeros(4,7,401);
for j = 1:1
    
    Q_Matrix = positionDeviation_costMatrix;
    R_Matrix = eye(carNum);
    BMatrix = Input_Matrix;
    for m = 1:401
        BMatrix(1,1) = 10*exp(-0.02*m);
        K = Riccati(AMatrix,BMatrix*inv(R_Matrix)*BMatrix',Q_Matrix);
    
    % The corresponding control would be equal to
        Gain_Matrix(:,:,m) = inv(R_Matrix)*BMatrix'*K;
    end
    
    
%%    
    
    x0 = zeros(2*carNum-1,1);
    % Initial conditions
    for i = 1:2*carNum-1
        if rem(i,2) == 1
            x0(i) = 0;
        else
            x0(i) = 6;
        end
    end
    
    x0(2) = 3;
    x0(4) = 7;
    
    TimeInterval =0:0.05:20;
    N = size(TimeInterval,2);
    
    
    x = zeros(2*carNum-1,N);
    dt = 0.05;
    x(:,1) = x0;
    for i = 1:N-1
        x(:,i+1) = x(:,i) + dt*(AMatrix-BMatrix*Gain_Matrix(:,:,i))*x(:,i);
    end
    
    %
    figure(1);
    
    for i = 1:2:2*carNum-1
        
        plot(TimeInterval,x(i,:));
        hold on;
    end
    xlabel('Time(s)','FontSize', 16);
    ylabel('Velocity Deviation','FontSize', 16);
    legend('Velocity deviation #1','Velocity deviation #2', 'Velocity deviation #3', 'Velocity deviation #4');
    % ca = sprintf('Coefficent %d', Cft);
    % h = legend(ca);
    % set(h,'FontSize',12);
    grid on;
    
    
    
    %
    figure(2);
    
    for i = 1:2:2*carNum-3
        plot(TimeInterval,x(i+1,:));
        hold on;
    end
    xlabel('Time(s)','FontSize', 16);
    ylabel('Position Deviation','FontSize', 16);
    legend('Position deviation #1','Position deviation #2', 'Position deviation #3');
    % ca = sprintf('Coefficent %d', Cft);
    % h = legend(ca);
    % set(h,'FontSize',12);
    grid on;
    
    
end


