% HW simulation for optimal control
% Here I use an arbitary linear system and use different {Q,R} to see the
% influence of LQR on the system response

A = [-6 -11 -6;1 0 0;0 1 0];
B = [1 0 0]';
% Here we set Q to be identity 
Q = eye(3);

% Using two different values 0.1 & 0.01 of R to see the influences under different
% values on the regulator

RS = [0.01 0.1 0.5];



%% Using the tool box inside

for i = 1:3
    R = RS(i);
    K = lqr(A,B,Q,R);
    
    A = [-6 -11 -6;1 0 0;0 1 0];
    B = [1 0 0]';
    N = ones(3,1);
    % Here we set Q to be identity and check the influence of R on the
    % regulator
    Q = eye(3);
    
    K = lqr(A,B,Q,R);
    
    A1 = A-B*K;
    C=eye(3);
    D=0;
    sys0 = ss(A,B,C,D);
    sys1 = ss(A1,B,C,D);
    figure;
    step(sys0);
    hold on;
    step(sys1,'--');
    legend('System Respons','LQR response'); 
end



